
 ภาค 4 ของที่ได้รับยกเว้นอากร  
==
 
  
## รายการสุทธินำกลับ ประเภทที่ 1 ภาค 4   
  
ของที่ส่งออกรวมทั้งของที่ส่งกลับออกไป ซึ่งนำกลับเข้ามา **ภายในหนึ่งปี** _โดยไม่เปลี่ยนแปลงลักษณะหรือรูปแต่ประการใด_ และในเวลาที่ส่งออกนั้นได้รับใบสุทธิสำหรับนำกลับเข้ามาแล้ว ได้รับยกเว้นอากร  ให้ผู้ขอยกเว้นอากรจัดทำและส่งข้อมูลใบขนสินค้าขาเข้า โดยระบุค่าดังนี้  

- ระบุ Re - Importation Certificate = Y ที่ส่วนรายการในใบขนสินค้า  	
- การสำแดงพิกัดศุลกากรและรหัสสถิติสินค้าให้สำแดงตรงกับชนิดของของที่นำเข้า  
- ระบุ Import Tariff = **1PART4**
- ระบุ Privilege Code = ตามสิทธิ ณ ขณะนำเข้า  
- อัตราอากร ใช้ Tariff Code และ Tariff Sequence
- อากร ยกเว้น (บันทึก Exemption Rate = 100%)
	 VAT ยกเว้น (บันทึก Exemption Rate = 100%)
- มูลค่าการนำเข้า CIF = **ค่าของ**
- บันทึกเลขที่ใบขนสินค้าขาออกและรายการที่ของใบขนสินค้าขาออกที่อ้างถึง



## รายการนำกลับไปซ่อม ประเภทที่ 2 ภาค 4 

ของที่นำเข้ามาในประเทศไทยซึ่งได้เสียอากรไว้ครบถ้วนแล้ว และภายหลัง**ส่งกลับออกไปซ่อม ณ ต่างประเทศ หากนำกลับเข้ามาภายในหนึ่งปี**หลังจากที่ได้รับใบสุทธิสำหรับนำกลับเข้ามาซึ่งได้ออกให้ในขณะที่ได้ส่งออกแล้ว ได้รับยกเว้นอากร

ของที่ได้รับยกเว้นอากรตามประเภทนี้ **ให้ได้รับยกเว้นเพียงเท่าราคาหรือปริมาณ แห่งของเดิมที่ส่งออกไปเท่านั้น สำหรับราคาหรือปริมาณที่เพิ่มขึ้นเนื่องจากการซ่อม ให้เสียอากรตามพิกัดอัตราอากรของของเดิมที่ส่งออกไปซ่อม โดยคำนวณจากราคาหรือปริมาณที่เพิ่มขึ้น ทั้งนี้ไม่รวมค่าใช้จ่ายในการขนส่งและการประกันภัย** ให้ผู้ขอยกเว้นอากรจัดทำและส่งข้อมูลใบขนสินค้าขาเข้า โดยระบุค่าดังนี้

- ระบุ Re - Importation Certificate = Y ที่ส่วนรายการในใบขนสินค้า
- การสำแดงพิกัดศุลกากรและรหัสสถิติสินค้าให้สำแดงตรงกับชนิดของของที่นำเข้า
- ระบุ Import Tariff = **2PART4**
- ระบุ Privilege Code =  000  (ใช้อัตราอากรที่ลดหย่อนเป็นการทั่วไปเท่านั้น)
- อัตราอากร ใช้ Tariff Code และ Tariff Sequence
- อากร ชำระตามปกติ
- VAT ชำระตามปกติ
- มูลค่าการนำเข้า  CIF = **ค่าซ่อม**
- บันทึกเลขที่ใบขนสินค้าขาออกและรายการที่ของใบขนสินค้าขาออกที่อ้างถึง


## สิทธิทัณฑ์บนนำเข้าชั่วคราว ประเภทที่ 3 ภาค 4 

ของนำเข้ามาพร้อมกับตนหรือนำเข้ามาเป็นการชั่วคราว และจะส่งกลับออกไปภายใน**ไม่เกิน  6 เดือน** นับตั้งแต่วันที่นำเข้ามา ได้รับยกเว้นอากร ให้ผู้ขอยกเว้นอากรจัดทำและส่งข้อมูลใบขนสินค้าขาเข้า โดยระบุค่าดังนี้

- ระบุ Import Tariff = **3PART4**
- ระบุ Privilege Code = ตามสิทธิ ณ ขณะนำเข้า 
- อัตราอากร ใช้ Tariff Code และ Tariff Sequence 
- อากร ยกเว้น (บันทึก Exemption Rate = 100%)
- VAT ยกเว้น (บันทึก Exemption Rate = 100%)

การกำหนดค่าวางเงินประกัน **(วางประกันที่ด่านศุลกากร ไม่ต้องบันทึกในโปรแกรม)**

- **_ให้วางเป็นสัญญาประกันทัณฑ์บนเพื่อการปฏิบัติพิธีการ_** 
- คำนวณจาก ค่าภาษีอากรอันพึงจะต้องชำระตามปกติ แล้วคำนวณเงินเพิ่มอีกร้อยละ 20 ของเงินภาษีอากรที่ได้ประเมินไว้ และรวมกำหนดเป็นเงินประกัน 

ให้ยื่นคำร้องต่อหน่วยงานศุลกากร **ณ ด่านศุลกากร ที่นำของเข้า** โดยระบุเลขที่ใบขนสินค้าขาเข้าที่ของใช้สิทธิตามประเภทที่ 3 ภาค 4 เพื่อตรวจสอบการใช้สิทธิในการนำของที่นำเข้ามาเป็นการชั่วคราว และจะต้องแสดงหลักฐานให้เป็นที่พอใจว่า นำเข้ามาใช้ประโยชน์ในกิจการตามที่ระบุไว้ ตามพิกัดอัตราศุลกากรแต่ละประเภท



| ประเภท| รายละเอียดทัณฑ์บนชั่วคราว |
|:-------:|:------------------------------------------------------------------|
| **(ก)** | ของที่ใช้ในการแสดงละครหรือการแสดงอย่างอื่นที่คล้ายกัน ซึ่งผู้แสดงที่ท่องเที่ยวนำเข้ามา| 
| **(ข)** | เครื่องประกอบและของใช้ในการทดลอง หรือการแสดงเพื่อวิทยาศาสตร์ หรือการศึกษา ซึ่งบุคคลที่เข้ามาในราชอาณาจักรเป็นการชั่วคราวนำเข้ามาเพื่อจัดการทดลองหรือแสดง| 
| **(ค)**|รถสำหรับเดินบนถนน เรือและอากาศยาน บรรดาที่เจ้าของนำเข้ามาพร้อมกับตน| 
|**(ง)**|  เครื่องถ่ายรูปและเครื่องบันทึกเสียง ซึ่งบุคคลที่เข้ามาในราชอาณาจักรเป็นการชั่วคราวนำเข้ามาเพื่อใช้ถ่ายรูปหรือบันทึกเสียงต่างๆ แต่ฟิล์มและแผ่นสำหรับถ่ายรูปหรือสิ่งที่ใช้บันทึกเสียง ซึ่งนำมาใช้ในการนี้ ต้องเป็นไปตามเงื่อนไข และปริมาณที่รัฐมนตรีว่าการกระทรวงการคลังกำหนด | 
| **(จ)**|  อาวุธปืนและกระสุนปืน ซึ่งบุคคลที่เข้ามาในราชอาณาจักรเป็นการชั่วคราวนำเข้ามา  พร้อมกับตน| 
| **(ฉ)** | ของที่นำเข้ามาเป็นการชั่วคราว  โดยมุ่งหมายจะแสดงในงานสาธารณะที่เปิดให้ประชาชนดูได้ทั่วไป| 
| **(ช)** | ของที่นำเข้ามาเพื่อซ่อม แต่ต้องปฏิบัติภายในเงื่อนไขที่อธิบดีกรมศุลกากรกำหนด| 
| **(ซ)** | ตัวอย่างสินค้า นอกจากที่ระบุไว้ในประเภทที่ 14 ซึ่งบุคคลที่เข้ามาในราชอาณาจักรเป็นการชั่วคราว นำเข้ามาพร้อมกับตนและมีสภาพ ซึ่งเมื่อจะส่งกลับออกไปสามารถตรวจได้แน่นอนว่า เป็นของอันเดียวกับที่นำเข้ามา แต่ต้องมีปริมาณ หรือค่าซึ่งเมื่อรวมกันเข้าแล้วไม่เกินกว่าที่จะเห็นได้ว่าเป็นตัวอย่างตามธรรมดา| 
| **(ญ)**|  เครื่องมือ และสิ่งประกอบสำหรับงานก่อสร้าง งานพัฒนาการ รวมทั้งกิจการชั่วคราวอย่างอื่นตามที่อธิบดีกรมศุลกากรจะเห็นสมควร โดยให้ได้รับยกเว้นเพียงเท่าเงินอากรที่จะพึงเสียในขณะนำเข้าหักด้วยจำนวนเงินอากรที่คำนวณตามระยะเวลาที่ของนั้นอยู่ในประเทศ ในอัตราร้อยละ 1 ต่อเดือนของจำนวนอากรที่จะพึงต้องเสียในขณะนำเข้า ในการคำนวณให้นับเศษของเดือนเป็น 1 เดือน และจะต้องชำระอากรก่อนส่งกลับออกไป| 


## สิทธิของใช้ส่วนตัว (ของส่วนตัว) ประเภทที่ 5 ภาค 4 

ของส่วนตัว ที่เจ้าของนำเข้ามาพร้อมกับตนสำหรับใช้เองหรือใช้ในวิชาชีพ และมีจำนวนพอสมควรแก่ฐานะ ได้รับยกเว้นอากร **เว้นแต่ รถยนต์ อาวุธปืนและกระสุนปืน และเสบียง**

**สุรา หรือ CIGARETTE หรือ CIGAR หรือยาเส้น** ซึ่งเป็นของส่วนตัวที่ผู้เดินทางนำเข้ามาพร้อมกับตนนั้น ยกเว้นอากรให้ได้ แต่ต้องไม่เกินปริมาณ ดังนี้

-  **CIGARETTE 200 มวน หรือ CIGAR หรือยาเส้น อย่างละ 250 กรัม หรือหลายชนิดรวมกันมีน้ำหนัก ทั้งหมด 250 กรัม แต่ทั้งนี้ CIGARETTE ต้องไม่เกิน 200 มวน**
-  **สุรา 1 ลิตร**   

ของส่วนตัวตามประเภทที่ 5 จะต้อง**นำเข้ามาถึงประเทศไทยไม่เกิน 1 เดือน**ก่อนที่ผู้นำของเข้ามาถึง หรือ**ไม่เกิน 6 เดือนนับแต่วันที่ผู้นำของเข้าเข้ามาถึง** ให้ผู้ได้รับยกเว้นอากรจัดทำและส่งข้อมูลใบขนสินค้าขาเข้า โดยระบุค่าดังนี้

 - การสำแดงพิกัดศุลกากรและรหัสสถิติสินค้าให้สำแดงตรงกับชนิดของของที่นำเข้า 
 - ระบุ Import Tariff = **5PART4** (ของส่วนตัวที่เจ้าของนำเข้ามาพร้อมกับตน)
 - ระบุ Privilege Code = ตามสิทธิ ณ ขณะนำเข้า 
 - อัตราอากร ใช้ Tariff Code และ Tariff Sequence 
 - อากร ยกเว้น (บันทึก Exemption Rate = 100%)
 - VAT ยกเว้น (บันทึก Exemption Rate = 100%)


## สิทธิของใช้ส่วนตัว (ของใช้ในบ้านเรือนที่ใช้แล้ว) ประเภทที่ 6 ภาค 4 

**ของใช้ในบ้านเรือนที่ใช้แล้ว ที่เจ้าของนำเข้ามาพร้อมกับตน เนื่องในการย้ายภูมิลำเนาและมีจำนวนพอสมควรแก่ฐานะ** ได้รับยกเว้นอากร ของใช้ในบ้านเรือนตามประเภทที่ 6 จะต้องนำเข้ามาถึงประเทศไทยไม่เกิน 1 เดือนก่อนที่ผู้นำของเข้ามาถึง หรือไม่เกิน 6 เดือนนับแต่วันที่ผู้นำของเข้าเข้ามาถึง ให้ผู้ได้รับยกเว้นอากรจัดทำและส่งข้อมูลใบขนสินค้าขาเข้า โดยระบุค่าดังนี้

- การสำแดงพิกัดศุลกากรและรหัสสถิติสินค้าให้สำแดงตรงกับชนิดของของที่นำเข้า 
- ระบุ Import Tariff = **6PART4** 
- ระบุ Privilege Code = ตามสิทธิ ณ ขณะนำเข้า 
- อัตราอากร ใช้ Tariff Code และ Tariff Sequence 
- อากร ยกเว้น (บันทึก Exemption Rate = 100%)
- VAT ยกเว้น (บันทึก Exemption Rate = 100%)



## สิทธิของนำเข้าส่วนประกอบและอุปกรณ์ประกอบของอากาศยานหรือเรือ

ของที่นำเข้ามาเพื่อใช้ซ่อมสร้างอากาศยานหรือเรือจะได้รับการยกเว้นอาการสำหรับส่วนประกอบ อุปกรณ์ประกอบ วัสดุที่นำเข้ามาเพื่อใช้ซ่อมหรือสร้างอากาศยาน หรือส่วนของอากาศยานให้ผู้ได้รับยกเว้นอากรจัดทำและส่งข้อมูลใบขนสินค้าขาเข้า โดยระบุค่าดังนี้

- การสำแดงพิกัดศุลกากรและรหัสสถิติสินค้าให้สำแดงตรงกับชนิดของของที่นำเข้า 
- ระบุ Import Tariff = **7PART4** (ของซ่อมสร้างอากาศยานหรือเรือ)
- ระบุ Privilege Code = ตามสิทธิ ณ ขณะนำเข้า 
- อัตราอากร ใช้ Tariff Code และ Tariff Sequence 
- อากร ยกเว้น (บันทึก Exemption Rate = 100%)
- VAT ยกเว้น (บันทึก Exemption Rate = 100%)


## สิทธิของนำเข้าที่ได้รับเอกสิทธิ์ ประเภทที่ 10 ภาค 4 


ของที่ได้รับเอกสิทธิ์ ตามข้อผูกพันที่ประเทศไทยมีอยู่ต่อองค์การสหประชาชาติ หรือตามกฎหมายระหว่างประเทศ หรือตามสัญญากับนานาประเทศ หรือทางการทูต ซึ่งได้ปฏิบัติต่อกันโดยอัธยาศัยไมตรี ได้รับยกเว้นอากรให้ผู้ได้รับยกเว้นอากรจัดทำและส่งข้อมูลใบขนสินค้าขาเข้า โดยระบุค่าดังนี้**

- การสำแดงพิกัดศุลกากรและรหัสสถิติสินค้าให้สำแดงตรงกับชนิดของของที่นำเข้า 
- ระบุ Import Tariff = **10PART4** (ของที่ได้รับเอกสิทธิ์)
- ระบุ Privilege Code = ตามสิทธิ ณ ขณะนำเข้า 
- อัตราอากร ใช้ Tariff Code และ Tariff Sequence 
- อากร ยกเว้น (บันทึก Exemption Rate = 100%)
- VAT ยกเว้น (บันทึก Exemption Rate = 100%)

กรณีจัดทำ **คำร้องขอรับของไปก่อน** แทนการจัดทำใบขนสินค้าขาเข้า

1. กรณีมีความจำเป็นต้องนำออกไปจากอารักขาของศุลกากรโดยรีบด่วน
2. ยังไม่สามารถดำเนินการเรื่องยกเว้นอากรได้แล้วเสร็จ 
3. ให้วางเงินสด หรือหนังสือค้ำประกันของธนาคาร หรือหนังสือของส่วนราชการเพื่อเป็นการประกันค่าภาษีอากร

**ให้ผู้นำเข้าจัดทำและส่งข้อมูลคำร้องขอรับของไปก่อน โดยระบุค่าดังนี้**

-	**Document Type = 3 (คำร้องขอรับของไปก่อน)**
-	การสำแดงพิกัดศุลกากรและรหัสสถิติสินค้าให้สำแดงตรงกับชนิดของของที่นำเข้า 
-	ระบุ Import Tariff = **10PART4** (ของที่ได้รับเอกสิทธิ์)
-	ระบุ Privilege Code = ตามสิทธิ ณ ขณะนำเข้า 
-	อัตราอากร ใช้ Tariff Code และ Tariff Sequence 
-	การวางประกัน ให้บันทึกเงินวางประกันในช่อง Deposit Amount 

![10PART4](https://github.com/yosarawut/WorkingArea/raw/master/KnowledgeCenter/e-Customs/img/4-10.png)

ในทางปฏิบัติ สำนักพิกัดอัตราศุลกากร โดยฝ่ายมาตรฐานพิกัดอัตราศุลกากรที่ 2.2 ส่วนมาตรฐานพิกัดอัตราศุลกากร 2 มีภารกิจหน้าที่ และความรับผิดชอบในการพิจารณาคำขออนุมัติยกเว้นอากรตามประเภท 10 เฉพาะกรณี
- ของที่ได้รับเอกสิทธิตามสัญญากับนานาประเทศและไม่ผ่านกรมความร่วมมือระหว่างประเทศ
- ของที่คณะกรรมการพิจารณายกเว้นอากรนำเข้าสื่อ วัสดุ เครื่องมือและอุปกรณ์ทางการศึกษา ภายใต้ความตกลงฟลอเรนซ์ ของยูเนสโก ได้พิจารณาและเสนอกรมศุลกากรเพื่อยกเว้นอากร

เมื่อฝ่ายมาตรฐานพิกัดอัตราศุลกากรที่ 2.2 ส่วนมาตรฐานพิกัดอัตราศุลกากร 2 ได้พิจารณาคำขออนุมัติยกเว้นอากรแล้วจะส่งผลการพิจารณา ไปยังสำนักงาน หรือ ด่านศุลกากร ณ ท่าหรือที่ ที่นำเข้าเพื่อปฏิบัติพิธีการศุลกากรต่อไป

>สอบถามข้อมูลเพิ่มเติมได้ที่ : ส่วนมาตรฐานพิกัดอัตราศุลกากร 2 (สพศ.2) กองพิกัดอัตราศุลกากร (กพก.) โทร.  0-2667-7000 ต่อ 20-5854 หรือ 5859
[**[Reference]**](http://www.customs.go.th/list_strc_simple_step.php?ini_content=customs_tariff_exemption_01&ini_menu=menu_interest_and_law_160421_01&lang=th&root_left_menu=menu_customs_tariff_exemption&left_menu=menu_customs_tariff_exemption_01)

## สิทธิของนำเข้ามาเพื่อบริจาค ประเภทที่ 11 ภาค 4 


ของที่นำเข้ามาหรือส่งออกไปเพื่อบริจาคเป็นสาธารณกุศลแก่ประชาชนโดยผ่านส่วนราชการหรือองค์การสาธารณกุศล **(ยกเว้นรถยนต์)** ได้รับยกเว้นอากร ให้ผู้ได้รับยกเว้นอากรจัดทำและส่งข้อมูลใบขนสินค้าขาเข้า โดยระบุค่าดังนี้

* การสำแดงพิกัดศุลกากรและรหัสสถิติสินค้าให้สำแดงตรงกับชนิดของของที่นำเข้า 
* ระบุ Import Tariff = **11PART4** (ของบริจาค)
* ระบุ Privilege Code = ตามสิทธิ ณ ขณะนำเข้า 
* อัตราอากร ใช้ Tariff Code และ Tariff Sequence 
* อากร ยกเว้น (บันทึก Exemption Rate = 100%)
* VAT ยกเว้น (บันทึก Exemption Rate = 100%)
* ระบุ Permit No = เลขที่ขอยกเว้นอากร ที่ได้รับ

กรณีจัดทำ**คำร้องขอรับของไปก่อน** แทนการจัดทำใบขนสินค้าขาเข้า

1. กรณีมีความจำเป็นต้องนำออกไปจากอารักขาของศุลกากรโดยรีบด่วน
2.  ยังไม่สามารถดำเนินการเรื่องยกเว้นอากรได้แล้วเสร็จ 
3.  ให้วางเงินสด หรือหนังสือค้ำประกันของธนาคาร หรือหนังสือของส่วนราชการเพื่อเป็นการประกันค่าภาษีอากร

**ให้ผู้นำเข้าจัดทำและส่งข้อมูลคำร้องขอรับของไปก่อน โดยระบุค่าดังนี้**

-	Document Type = 3 (คำร้องขอรับของไปก่อน)
-	การสำแดงพิกัดศุลกากรและรหัสสถิติสินค้าให้สำแดงตรงกับชนิดของของที่นำเข้า 
-	ระบุ Import Tariff = **11PART4** (ของบริจาค)
-	ระบุ Privilege Code = ตามสิทธิ ณ ขณะนำเข้า 
-	อัตราอากร ใช้ Tariff Code และ Tariff Sequence 
-	การวางประกัน ให้บันทึกเงินวางประกันในช่อง Deposit Amount 

![11PART4](https://github.com/yosarawut/WorkingArea/raw/master/KnowledgeCenter/e-Customs/img/4-11.png)

ในทางปฏิบัติ สำนักพิกัดอัตราศุลกากร โดยฝ่ายมาตรฐานพิกัดอัตราศุลกากรที่ 2.2 ส่วนมาตรฐานพิกัดอัตราศุลกากร 2 มีภารกิจหน้าที่และความรับผิดชอบในการพิจารณาคำขออนุมัติยกเว้นอากรตามประเภท 11 ในทุกกรณี  
  
เมื่อฝ่ายมาตรฐานพิกัดอัตราศุลกากรที่ 2.2 ส่วนมาตรฐานพิกัดอัตราศุลกากร 2ได้พิจารณาคำขออนุมัติยกเว้นอากรแล้วจะส่งผลการพิจารณาไปยังสำนักงาน หรือ ด่านศุลกากร ณ ท่าหรือที่ ที่นำเข้าหรือส่งออกเพื่อปฏิบัติพิธีการศุลกากรต่อไป

  

**กฎหมาย/ระเบียบที่เกี่ยวข้อง**

1. [ประกาศกรมศุลกากร ที่ 81/2551 เรื่อง หลักเกณฑ์และเงื่อนไขสำหรับของตามภาค 4 ประเภท 11](http://www.customs.go.th/data_files/c86b11103d848f53e025fedf13e3c145.pdf)

2. [ประกาศกรมศุลกากร ที่ 1/2558 เรื่อง ระเบียบปฏิบัติสำหรับการขอยกเว้นอากรของตามภาค 4 ประเภท 11 แห่งพระราชกำหนดพิกัดอัตราศุลกากร พ.ศ. 2530](http://www.customs.go.th/data_files/39e15b57c90cf9f3dd5bf87c15754d32.pdf)

>สอบถามข้อมูลเพิ่มเติมได้ที่ : ส่วนมาตรฐานพิกัดอัตราศุลกากร 2 (สพศ.2) กองพิกัดอัตราศุลกากร (กพก.) โทร.  0-2667-7000 ต่อ 20-5854 หรือ 5859
[**[Reference]**](http://www.customs.go.th/list_strc_simple_step.php?ini_content=customs_tariff_exemption_02&ini_menu=menu_interest_and_law_160421_01&lang=th&root_left_menu=menu_customs_tariff_exemption&left_menu=menu_customs_tariff_exemption_02)

## ยุทธภัณฑ์ที่ใช้ในราชการ ประเภทที่ 13 ภาค 4 


**ให้ผู้ได้รับยกเว้นอากรจัดทำและส่งข้อมูลใบขนสินค้าขาเข้า โดยระบุค่าดังนี้**

- การสำแดงพิกัดศุลกากรและรหัสสถิติสินค้าให้สำแดงตรงกับชนิดของของที่นำเข้า 
- ระบุ Import Tariff = **13PART4** (ยุทธภัณฑ์ที่ใช้ในราชการ)
- ระบุ Privilege Code = ตามสิทธิ ณ ขณะนำเข้า 
- อัตราอากร ใช้ Tariff Code และ Tariff Sequence
- อากร ยกเว้น (บันทึก Exemption Rate = 100%)
- VAT ยกเว้น (บันทึก Exemption Rate = 100%)

 
**กรณีจัดทำคำร้องขอรับของไปก่อน แทนการจัดทำใบขนสินค้าขาเข้า**

1. กรณีมีความจำเป็นต้องนำออกไปจากอารักขาของศุลกากรโดยรีบด่วน
2. ยังไม่สามารถดำเนินการเรื่องยกเว้นอากรได้แล้วเสร็จ 
3. ให้วางเงินสด หรือหนังสือค้ำประกันของธนาคาร หรือหนังสือของส่วนราชการเพื่อเป็นการประกันค่าภาษีอากร

**ให้ผู้นำเข้าจัดทำและส่งข้อมูลคำร้องขอรับของไปก่อน โดยระบุค่าดังนี้**

-	Document Type = 3 (คำร้องขอรับของไปก่อน)
-	การสำแดงพิกัดศุลกากรและรหัสสถิติสินค้าให้สำแดงตรงกับชนิดของของที่นำเข้า 
-	ระบุ Import Tariff = **13PART4** (ยุทธภัณฑ์ที่ใช้ในราชการ)
-	ระบุ Privilege Code = ตามสิทธิ ณ ขณะนำเข้า 
-	อัตราอากร ใช้ Tariff Code และ Tariff Sequence
-	การวางประกัน ให้บันทึกเงินวางประกันในช่อง Deposit Amount  

![13PART4](https://github.com/yosarawut/WorkingArea/raw/master/KnowledgeCenter/e-Customs/img/4-13.png)

ในทางปฏิบัติ สำนักพิกัดอัตราศุลกากร โดยฝ่ายมาตรฐานพิกัดอัตราศุลกากรที่ 2.2 ส่วนมาตรฐานพิกัดอัตราศุลกากร 2มีภารกิจหน้าที่และความรับผิดชอบในการพิจารณาคำขออนุมัติยกเว้นอากรตามประเภท 13 ที่ได้รับต้นเรื่องพร้อมคำขออนุมัติยกเว้นอากรจาก สำนักงาน หรือด่านศุลกากร ณ ท่าหรือที่ ที่นำเข้าเฉพาะกรณี
1. ที่มิใช่การนำเข้าโดยกระทรวงกลาโหม สำนักงานตำรวจแห่งชาติ กรมศุลกากร กรมสรรพสามิต กรมการปกครอง หรือคู่สัญญากับหน่วยงานข้างต้นเหล่านี้
2. ของที่สำนักงาน หรือ ด่านศุลกากร ณ ท่าหรือที่ ที่นำเข้ามีปัญหาและไม่อาจพิจารณาได้

เมื่อฝ่ายมาตรฐานพิกัดอัตราศุลกากรที่ 2.2 ส่วนมาตรฐานพิกัดอัตราศุลกากร 2ได้พิจารณาคำขออนุมัติยกเว้นอากรแล้วจะส่งผลการพิจารณาไปยังสำนักงาน หรือ ด่านศุลกากร ณ ท่าหรือที่ ที่นำเข้าเพื่อปฏิบัติพิธีการศุลกากรต่อไป 

**กฎหมาย/ระเบียบที่เกี่ยวข้อง**

1. [พระราชบัญญัติควบคุมยุทธภัณฑ์ พ.ศ. 2530](http://th.customs.go.th/data_files/1607071621221317251454.pdf)

2. [ประกาศกรมศุลกากร ที่ 80/2551ระเบียบปฏิบัติเกี่ยวกับการยกเว้นอากรสำหรับของตามพระราชกำหนด](http://th.customs.go.th/data_files/1607071623141617979603.pdf)[พิกัดอัตราศุลกากร พ.ศ. 2530 ภาค 4 ประเภทที่ 13](http://th.customs.go.th/data_files/1607071623141617979603.pdf)

>สอบถามข้อมูลเพิ่มเติมได้ที่ : ส่วนมาตรฐานพิกัดอัตราศุลกากร 2 (สพศ.2) กองพิกัดอัตราศุลกากร (กพก.) โทร.  0-2667-7000 ต่อ 20-5854 หรือ 5859
[**[Reference]**](http://www.customs.go.th/list_strc_simple_step.php?ini_content=customs_tariff_exemption_03&ini_menu=menu_interest_and_law_160421_01&lang=th&root_left_menu=menu_customs_tariff_exemption&left_menu=menu_customs_tariff_exemption_03)

## ตัวอย่างสินค้า ประเภทที่ 14 ภาค 4 

**ให้ผู้ได้รับยกเว้นอากรจัดทำและส่งข้อมูลใบขนสินค้าขาเข้า โดยระบุค่าดังนี้**

- การสำแดงพิกัดศุลกากรและรหัสสถิติสินค้าให้สำแดงตรงกับชนิดของของที่นำเข้า 
- ระบุ Privilege Code = ตามสิทธิ ณ ขณะนำเข้า 
- อัตราอากร ใช้ Tariff Code และ Tariff Sequence
- อากร ยกเว้น (บันทึก Exemption Rate = 100%)
- VAT ยกเว้น (บันทึก Exemption Rate = 100%)



## สิทธิของนำเข้ามาสำหรับคนพิการ ประเภทที่ 16 ภาค 4 

ของที่นำเข้ามาสำหรับคนพิการใช้โดยเฉพาะ หรือใช้ในกรณีฟื้นฟูสมรรถภาพคนพิการ ได้รับยกเว้นอากร  ให้ผู้ได้รับยกเว้นอากรจัดทำและส่งข้อมูลใบขนสินค้าขาเข้า โดยระบุค่าดังนี้

* การสำแดงพิกัดศุลกากรและรหัสสถิติสินค้าให้สำแดงตรงกับชนิดของของที่นำเข้า 
* ระบุ Import Tariff = **16PART4** ของที่นำเข้ามาสำหรับคนพิการ
* ระบุ Privilege Code = ตามสิทธิ ณ ขณะนำเข้า 
* อัตราอากร ใช้ Tariff Code และ Tariff Sequence
* อากร ยกเว้น (บันทึก Exemption Rate = 100%)
* VAT ยกเว้น (บันทึก Exemption Rate = 100%)
 

## สิทธิของนำเข้ามาสำหรับการประชุมระหว่างประเทศ 

การยกเว้นอากรสำหรับของที่นำเข้ามา เพื่อใช้ในการประชุมระหว่างประเทศโดยได้รับยกเว้นอากรให้ปฏิบัติตามหลักเกณฑ์และเงื่อนไขดังต่อไปนี้

1. เป็นของที่นำเข้ามาเพื่อใช้ในการประชุมระหว่างประเทศ ซึ่งจัดขึ้นโดยหน่วยงานของรัฐบาลไทย หรือของรัฐบาลต่างประเทศ องค์การระหว่างประเทศที่รัฐบาลไทยได้รับรอง หรือโดยหน่วยงานของเอกชนที่การประชุมนั้น เป็นประโยชน์แก่การพัฒนาทางเศรษฐกิจ ทางสังคม หรือทางเทคโนโลยีของประเทศ 
2. เป็นของจำเป็นที่ใช้ในการประชุม ได้แก่ แฟ้มเอกสาร เอกสาร สื่อบันทึกข้อมูล แบบพิมพ์ ของที่ระลึกที่มีมูลค่าแต่ละชิ้นเพียงเล็กน้อย เป็นต้น ซึ่งของต้องจัดทำจนเห็นได้ว่าเป็นของที่นำเข้าเพื่อแจกจ่ายแก่ผู้เข้าร่วมประชุม
3.  เป็นของซึ่งเตรียมมาโดยเฉพาะสำหรับใช้หมดสิ้นไปในการประชุม เช่น วัสดุที่ใช้ใน
การแสดง หรือทดลองประกอบการประชุม และมีปริมาณพอสมควร  
4.  ของอื่น ๆ นอกเหนือที่ระบุไว้ในข้อ (ข) และ (ค) ต้องส่งกลับออกไปภายใน 3 เดือนนับแต่วันนำเข้า โดยผู้นำของเข้าต้องทำสัญญาไว้ต่อกรมศุลกากรตามเงื่อนไขที่กำหนด

**ให้ผู้ได้รับยกเว้นอากรจัดทำและส่งข้อมูลใบขนสินค้าขาเข้า โดยระบุค่าดังนี้**

- การสำแดงพิกัดศุลกากรและรหัสสถิติสินค้าให้สำแดงตรงกับชนิดของของที่นำเข้า 
- ระบุ Import Tariff = **17PART4** ของที่นำเข้ามาเพื่อใช้ในการประชุมระหว่างประเทศ
- ระบุ Privilege Code = ตามสิทธิ ณ ขณะนำเข้า 
- อัตราอากร ใช้ Tariff Code และ Tariff Sequence
- อากร ยกเว้น (บันทึก Exemption Rate = 100%)
- VAT ยกเว้น (บันทึก Exemption Rate = 100%)


## ขอใช้สิทธินำเข้าเครื่องมือ เครื่องใช้ วัสดุอุปกรณ์ในการขุดเจาะน้ำมัน โดยขอยกเว้นอากรตาม พรบ.ปิโตรเลียม (กรมเชื้อเพลิงธรรมชาติ)

**การจัดทำข้อมูลคำร้องขอรับของไปก่อน โดยขอยกเว้นอากรตาม พระราชบัญญัติปิโตรเลียม พ.ศ.2514**

- บันทึก Document Type = **3** (คำร้องขอรับของไปก่อน)
- บันทึก Privilege Code = **005** (สำหรับรายการที่รอการยกเว้นอากร)
- บันทึกอัตราอากรใช้ที่ลดหย่อนเป็นการทั่วไป (**Privilege Code = 000** เท่านั้น)
- อากร ยกเว้น (บันทึก Exemption Rate = 100%)
- VAT ยกเว้น (บันทึก Exemption Rate = 100%)



<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE3MzkzMzczOTcsMjAyNDQwMDU4MV19
-->