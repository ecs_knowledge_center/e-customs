## ลำดับหัวข้อคู่มือขาเข้า


1. รูปแบบเลขที่ใบขนสินค้า
2. การตรวจสอบระบบทะเบียนผู้มาติดต่อ
3. วันนำเข้า
4. เลขที่ใบตราส่ง (Bill of Lading)
5. การกำหนดสถานที่
6. เครื่องหมายและเลขหมายหีบห่อ (Shipping Marks)
7. จำนวนและน้ำหนักของสินค้า
8. มีปัญหาในเรื่องต่าง ๆ ต้องการให้ตรวจสอบข้อมูลใบขนสินค้าขาเข้า 

	* [โครงสร้างข้อมูลใบขนสินค้าขาเข้า ](e-Import/Manual/โครงสร้่างข้อมูล.md)
	* [การตรวจสอบผลการคำนวณมูลค่าเงินนำเข้า](e-Import/Manual/การคำนวณมูลค่าเงิน.md)


	* [การตรวจสอบระบบทะเบียนผู้มาติดต่อ](e-Import/Manual/visitor-registration.md)
	* [วันนำเข้า](e-Import/Manual/import-date.md)
	* [เลขที่ใบตราส่ง (Bill of Lading)](e-Import/Manual/bill-of-lading.md)
	* [การกำหนดสถานที่](e-Import/Manual/customs-port.md)
	* [เครื่องหมายและเลขหมายหีบห่อ (Shipping Marks)](e-Import/Manual/shipping-marks.md)
	* [จำนวนและน้ำหนักของสินค้า](e-Import/Manual/package-weight.md)
	*
	* [](e-Import/Manual/โครงสร้่างข้อมูล.md)
	* [โครงสร้างข้อมูลใบขนสินค้าขาเข้า ](e-Import/Manual/โครงสร้่างข้อมูล.md)
	* [โครงสร้างข้อมูลใบขนสินค้าขาเข้า ](e-Import/Manual/โครงสร้่างข้อมูล.md)
	* [โครงสร้างข้อมูลใบขนสินค้าขาเข้า ](e-Import/Manual/โครงสร้่างข้อมูล.md)
	* [โครงสร้างข้อมูลใบขนสินค้าขาเข้า ](e-Import/Manual/โครงสร้่างข้อมูล.md)
	* [โครงสร้างข้อมูลใบขนสินค้าขาเข้า ](e-Import/Manual/โครงสร้่างข้อมูล.md)
	* 



















<!--stackedit_data:
eyJoaXN0b3J5IjpbMTE5MjgyMTczMiwxMTQ5ODg4MjgzLDQzOD
gzNjgyN119
-->