
## e-Import

## [คู่มือการปฏิบัติพิธีการศุลกากรสำหรับการนำเข้า (e-Import)](e-Import/Manual/scope.md)

* [ขอบเขตการใช้งาน](e-Import/Manual/overview.md)
* [**โครงสร้างข้อมูลใบขนสินค้าขาเข้า**](e-Import/Manual/data-structure.md)
	* [Declaration Control](e-Import/Manual/data-structure.md#import-declaration-control)	
	* [Invoice Control](e-Import/Manual/data-structure.md#import-invoice-control)
	* [Invoice Detail](e-Import/Manual/data-structure.md#import-invoice-detail)
	* [Invoice Detail (Duty)](e-Import/Manual/data-structure.md#import-invoice-duty)
	* [Invoice Detail (Permit)](e-Import/Manual/data-structure.md#import-invoice-permit)
	* [Invoice Detail (Deposit)](e-Import/Manual/data-structure.md#import-invoice-deposit)
* [การตรวจสอบผลการคำนวณมูลค่าเงินนำเข้า](e-Import/Manual/calculate-cif.md)
* [รูปแบบเลขที่ใบขนสินค้า](e-Import/Manual/declaration-no.md)
* [การตรวจสอบระบบทะเบียนผู้มาติดต่อ](e-Import/Manual/visitor-registration.md)
* [วันนำเข้า](e-Import/Manual/import-date.md)
* [เลขที่ใบตราส่ง (Bill of Lading)](e-Import/Manual/bill-of-lading.md)
* [การกำหนดสถานที่](e-Import/Manual/customs-port.md)
* [เครื่องหมายและเลขหมายหีบห่อ (Shipping Marks)](e-Import/Manual/shipping-marks.md)
* [จำนวนและน้ำหนักของสินค้า](e-Import/Manual/package-weight.md)
* [มีปัญหาในเรื่องต่าง ๆ ต้องการให้ตรวจสอบข้อมูลใบขนสินค้าขาเข้า](e-Import/Manual/assessment-request.md)
* [รหัสสกุลเงินตราและอัตราแลกเปลี่ยน](e-Import/Manual/currency-code.md)
* [หลักการรวมรายการของบัญชีราคาสินค้า](e-Import/Manual/include-item.md)
* [ราคาของในการจัดทำข้อมูลใบขนสินค้าขาเข้า](e-Import/Manual/cif-value.md)
* [INCOTERMS](e-Import/Manual/INCOTERMS.md)
* [ภาค 4 ของที่ได้รับยกเว้นอากร](e-Import/Manual/Part4.md)
* [สิทธิประโยชน์ทางภาษีอากร](e-Import/Manual/e-Tax_incentive.md)
* **การชำระหรือวางประกันค่าภาษีอากร**
	* [การขอวางประกันโต้แย้งพิกัดอัตราศุลกากร](e-Import/Manual/deposit-tariff.md)
	* [ขอวางประกันกองทุนน้ำมันเชื้อเพลิง](e-Import/Manual/deposit-oil.md)
	* [วางประกันอากรตอบโต้การทุ่มตลาดและการอุดหนุน](e-Import/Manual/deposit-dumping.md)
	* [หลักการบันทึกค่าภาษีอากรและวางประกัน](e-Import/Manual/tax-payment.md)
	* [Payment Method and Guarantee Method](e-Import/Manual/payment-method.md)
* [รหัสเหตุผลการโต้แย้ง (Argumentative Reason Code)](e-Import/Manual/argumentative-reason-code.md)
* [ประเภทค่าภาษีอากร (Duty Type)](e-Import/Manual/duty-type.md)
* [หน่วยงานผู้ออกใบอนุญาต](e-Import/Manual/permit-issue-authority.md)
* [การตรวจสอบระหว่างใบขนสินค้ากับ Manifest](e-Import/Manual/inspection-declaration-manifest.md)
* [รหัสสิทธิพิเศษ](e-Import/Manual/priviledge-code.md)

## บทความ
* [สรุปการปรับปรุงเปลี่ยนแปลง การดำเนินกระบวนการทางศุลกากร สำหรับสินค้าที่ต้องมีใบอนุญาต/ใบรับรอง](Articles/permission_09_2019.md)


<!--stackedit_data:
eyJoaXN0b3J5IjpbMjEzODE5ODQ2NiwtMTcxODcwMjY0NiwxOT
YwNTc4MDU1LC0xNjQyODI1NDg2LC0xMzAyMDkwMDg5LDE2MzUw
MzIxNzQsMTkyOTE3MDk0MywtMTc1MDQ4MzQ2MCwtMTU3ODQ3Mj
Q1OCwtMjEyMjMwMzE5NiwtNTA4MjU0MSwtMTYxNDE2ODkwNywy
MjIwODg1MTEsLTEzNDI0NjYwNzcsLTg1NDE4NjU2NSwtMTI5ND
c3NjkxOCwzMjM1NzAyODcsLTMyNzk3Nzc3LDE1NDk5OTEyMTUs
MjEyNzkzMTUyXX0=
-->